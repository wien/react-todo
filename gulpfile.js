"use strict";

const babel = require('gulp-babel');
const browserify = require('browserify');
const buffer = require('vinyl-buffer');
const clean = require('gulp-clean');
const clone = require('gulp-clone');
const es = require('event-stream');
const gulp = require('gulp');
const gulpIf = require('gulp-if');
const htmlmin = require('gulp-htmlmin');
const jshint = require('gulp-jshint');
const jshintJsx = require('jshint-jsx');
const livereload = require('gulp-livereload');
const plumber = require('gulp-plumber');
const stream = require('stream');
const rename = require("gulp-rename");
const runSequence = require("run-sequence");
const sass = require('gulp-sass');
const serve = require('gulp-serve');
const sourcemaps = require('gulp-sourcemaps');
const toVinylStream = require('vinyl-source-stream');

function lintStream() {
	return gulp.src([
		'assets/js/**/*.js',
		'gulpfile.js'
	]).pipe(
		plumber()
	).pipe(
		jshint({
			linter: jshintJsx.JSXHINT,
			esversion: 6,
			node: true,
			loopfunc: true
		})
	).pipe(
		jshint.reporter('jshint-stylish')
	);
}

function javascriptStream() {
	const destination = gulp.dest('./dist');
	const debug = true;

	browserify({
		entries: ['./assets/js/main.js'],
		debug: debug
	}).transform(
		'babelify',
		{
			presets: ['es2015', 'react']
		}
	).transform(
		'uglifyify',
		{
			global: !debug
		}
	).bundle().pipe(
		toVinylStream('main.js')
	).pipe(
		buffer()
	).pipe(
		plumber()
	).pipe(
		rename(path => {
			path.dirname =  path.dirname + '/js';
		})
	).pipe(
		destination
	);

	return destination;
}

function sassStream() {
	const destination = gulp.dest('./dist');
	const debug = true;

	gulp.src(
		'./assets/scss/*.scss'
	).pipe(
		plumber()
	).pipe(
		gulpIf(debug, sourcemaps.init())
	).pipe(
		sass({
			outputStyle: 'compressed'
		})
	).pipe(
		gulpIf(debug, sourcemaps.write())
	).pipe(
		rename(path => {
			path.dirname =  path.dirname + '/css';
		})
	).pipe(
		destination
	);

	return destination;
}

function siteStream() {
	const destination = gulp.dest('./dist');
	const debug = true;

	gulp.src(
		'./assets/html/**/*.html'
	).pipe(
		plumber()
	).pipe(
		gulpIf(!debug, htmlmin({collapseWhitespace: true}))
	).pipe(
		destination
	);

	return destination;
}

gulp.task('build', ['clean', 'lint'], () => {
	const source = es.merge(
		siteStream(),
		javascriptStream(),
		sassStream()
	);

	// Stop files at the fence until they have all arrived, then let all of
	// them through. This prevents livereload form triggering a reload of the
	// page while javascript or sass is still being built.
	const fence = es.pause();

	fence.pause();
	source.on('end', function () {
		fence.resume();
	});

	return source.pipe(
		fence
	).pipe(
		livereload()
	);
});

gulp.task('js', () => {
	return javascriptStream().pipe(
		livereload()
	);
});

gulp.task('lint', () => {
	return lintStream();
});


gulp.task('site', () => {
	return siteStream().pipe(
		livereload()
	);
});

gulp.task('sass', () => {
	return sassStream().pipe(
		livereload()
	);
});

gulp.task('clean', () => {
	return gulp.src(
		'./dist',
		{read: false}
	).pipe(
		clean()
	);
});

gulp.task('serve', serve({
	root: ['dist'],
	port: 9000
}));

gulp.task('watch', () => {
	livereload.listen({
		basePath: './dist'
	});

	gulp.watch(
		[
			'./assets/**/*',
			'./gulpfile.js',
		],
		[
			'build'
		]
	);
});

gulp.task('default', () => {
	return gulp.dest('build');
});
