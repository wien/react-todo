const Application = require('./components/application');
const $ = require('jquery');
global.jQuery = $;
const bs = require('bootstrap-sass');
const React = require('react');
const ReactDOM = require('react-dom');

ReactDOM.render(
  <Application />,
  document.querySelector('.container')
);
