const React = require('react');
const ReactDOM = require('react-dom');

class TodoItem extends React.Component {
	render() {
		const className = 'list-group-item' + (this.props.done ? ' disabled' : '');

		return (
			<a className={className}>{this.props.title}</a>
		);
	}
}

module.exports = TodoItem;
