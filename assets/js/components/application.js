const React = require('react');
const ReactDOM = require('react-dom');
const TodoItem = require('./todo-item');

class Application extends React.Component {
	constructor() {
		super();
		this.state = {
			list: [
				{
					title: 'Do this',
					done: false
				},
				{
					title: 'But not this',
					done: true
				},
				{
					title: 'And this',
					done: false
				}
			]
		};

		this.add = this.add.bind(this);
	}

	add() {
		const list = [{
			title: 'New item',
			done: false
		}].concat(this.state.list);

		this.setState({
			list: list
		});
	}

	markDone(index) {
		console.log(this, index);
	}

	render() {
		return (
			<div>
				<h2>TODO-list <button className="btn btn-primary pull-right" onClick={this.add}>Add</button></h2>
				<div className="list-group">
					{
						this.state.list.map((item, index) => {
							return <TodoItem
								title={item.title}
								done={item.done}
								key={index}
								onClick={this.markDone.bind(this, index)}
							/>;
						})
					}
				</div>
			</div>
		);
	}
}

module.exports = Application;
